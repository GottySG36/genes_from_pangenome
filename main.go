// Utility to extract speified pangenome gene category from pangenomme file

package main

import (
    "bufio"
    "flag"
    "fmt"
    "log"
    "os"
    "regexp"
    "strings"

    "bitbucket.org/GottySG36/pangenome"
)


type Description struct {
    Pangenome   string
    Cluster     string
    Outdir      string
}

func GetDesc(i, c, o string) Description {
    var d Description
    d.Pangenome = i
    d.Cluster   = c
    d.Outdir = o

    if d.Outdir == "" {
        d.Outdir = defaultOutDir
    }

    return d
}


// Given a ClusterID (key), return a list of genes
type Clusters map[string][]string

// Each line listing a cluster member starts with a number followed by a space and a column
var re_member = regexp.MustCompile(`^\d+\s:\s`)

func LoadClusters(f string, g map[string]bool) (Clusters, error) {
    o, err :=os.Open(f)
    defer o.Close()
    if err != nil {
        return nil, err
    }
    r := bufio.NewScanner(o)
    clust := make(Clusters,0)
    var c string

    for r.Scan() {
        if !re_member.MatchString( r.Text() ) {
            c = r.Text()
        } else if re_member.MatchString( r.Text() ) && g[c] {
            clust[c] = append(clust[c], re_member.ReplaceAllLiteralString(r.Text(), ""))
        }
    }
    return clust, nil
}

// This function splits all clusters into samples. It assumes that the required information
// to determine which cluster member came from where can be found in the sequence header.
// Sequence header will have different information seperated by a single character.
func (c Clusters) SplitSamples(s string, pos int) GeneList {
    samples := make(GeneList, 0)
    for _, glist := range c {
        for _, gene := range glist {
            sName := strings.Split(gene, s)[pos]
            samples[sName] = append(samples[sName], gene)
        }
    }
    return samples
}

// Given a SampleID (key), return a list of genes specific to that patient
type GeneList map[string][]string

func (g GeneList) Write(d Description) error{
    for sName, genes := range g {
        f, err := os.Create(fmt.Sprintf("%v/%v.txt", d.Outdir, sName))
        defer f.Close()
        if err != nil {
            return err
        }
        _, err = f.WriteString(strings.Join(genes, "\n"))
        if err != nil {
            return err
        }
    }
    return nil
}

// flag arguments definition for running the program
var (
    pange   = flag.String("p",      "-",        "Pangenome file from which to extract the relevant gene category")
    clust   = flag.String("c",      "",         "File containing the list of clusters along with their members")
    outd    = flag.String("o",      ".",        "Output directory in which to write the output files")
    cat     = flag.String("cat",    "",         "Gene category to extract (one of {core|acc|spec}")
    sep     = flag.String("s",      "|",        "Field delimiter used in sequence header. Use with '-pos' to indicate where the sample name is located in the header")
    pos     = flag.Int("pos",       1,          "Index of the element to use as sample name in the sequence's header (0-indexed). Used to determine where to write individual sequences.")

    uStr    = `genes_from_pangenome -c FILE -cat {core|acc|spec} [-p FILE] [-o DIR] [-s CHAR] [-pos INT]`
    descr   = `
This tool is designed to extract a specific group of genes from a pangenome. 
It works by going through a list of clusters for that group and return a list of 
genes for that cluster on a per sample basis.
`
    defaultOutDir = "."
)

var Usage = func() {
    fmt.Fprintf(flag.CommandLine.Output(), "%v\n\n", descr)
    fmt.Fprintf(flag.CommandLine.Output(), "Usage : %v\n\n", uStr)
    fmt.Fprintf(flag.CommandLine.Output(), "Arguments of %s:\n", os.Args[0])
    flag.PrintDefaults()
}

func main() {
    flag.Usage = Usage
    flag.Parse()

    if *cat != "core" && *cat != "acc" && *cat != "spec" {
        log.Fatalf("No category selected. You need to pick one of {core | acc | spec}")
    }

    desc := GetDesc(*pange, *clust, *outd)

    p, _, err := pangenome.LoadPangenome(desc.Pangenome)
    if err != nil {
        log.Fatalf("Error -:- LoadPangenome : %v\n", err)
    }

    var g map[string]bool

    switch *cat {
    case "core":
        g = p.CoreGenes()
    case "acc":
        g = p.AccGenes()
    case "spec":
        g = p.SpecGenes()
    }

    c, err := LoadClusters(desc.Cluster, g)
    if err != nil {
        log.Fatalf("Error -:- LoadClusters : %v\n", err)
    }

    s := c.SplitSamples(*sep, *pos)
    err = s.Write(desc)
    if err != nil {
        log.Fatalf("Error -:- Write : %v\n", err)
    }

}
